﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KolkoKrzyzyk
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ClearOptions();
        }

        private void ClearOptions()
        {
            Intro.Visibility = Visibility.Visible;
            menu.Visibility = Visibility.Visible;
            panelA.Visibility = Visibility.Hidden;
            panelS.Visibility = Visibility.Hidden;
            panelM.Visibility = Visibility.Hidden;
            panelMS.Visibility = Visibility.Hidden;
            panelMK.Visibility = Visibility.Hidden;
            game.Visibility = Visibility.Hidden;
            ZmienneG.playerO = "";
            ZmienneG.playerX = "";
            ZmienneG.turn = 0;
        }

        #region Menu
        private void powrotA_Click(object sender, RoutedEventArgs e)
        {
            menu.Visibility = Visibility.Visible;
            panelA.Visibility = Visibility.Hidden;
        }

        private void singlePlayer_Click(object sender, RoutedEventArgs e)
        {
            menu.Visibility = Visibility.Hidden;
            panelS.Visibility = Visibility.Visible;
        }

        private void multiPlayr_Click(object sender, RoutedEventArgs e)
        {
            menu.Visibility = Visibility.Hidden;
            panelM.Visibility = Visibility.Visible;
        }

        private void autor_Click(object sender, RoutedEventArgs e)
        {
            menu.Visibility = Visibility.Hidden;
            panelA.Visibility = Visibility.Visible;
        }

        private void powrotS_Click(object sender, RoutedEventArgs e)
        {
            menu.Visibility = Visibility.Visible;
            panelS.Visibility = Visibility.Hidden;
        }

        private void powrotM_Click(object sender, RoutedEventArgs e)
        {
            menu.Visibility = Visibility.Visible;
            panelM.Visibility = Visibility.Hidden;
        }

        private void server_Click(object sender, RoutedEventArgs e)
        {
            panelM.Visibility = Visibility.Hidden;
            panelMS.Visibility = Visibility.Visible;
        }

        private void klient_Click(object sender, RoutedEventArgs e)
        {
            panelM.Visibility = Visibility.Hidden;
            panelMK.Visibility = Visibility.Visible;
        }

        private void powrotMS_Click(object sender, RoutedEventArgs e)
        {
            panelM.Visibility = Visibility.Visible;
            panelMS.Visibility = Visibility.Hidden;
        }

        private void powrotMK_Click(object sender, RoutedEventArgs e)
        {
            panelM.Visibility = Visibility.Visible;
            panelMK.Visibility = Visibility.Hidden;
        }

        private void startMK_Click(object sender, RoutedEventArgs e)
        {

        }

        private void powrotSS_Click(object sender, RoutedEventArgs e)
        {
            game.Visibility = Visibility.Hidden;
            Intro.Visibility = Visibility.Visible;
            ClearOptions();
        }

        private void startS_Click(object sender, RoutedEventArgs e)
        {
            game.Visibility = Visibility.Visible;
            Intro.Visibility = Visibility.Hidden;
            Game();

        }

        #endregion

        #region Style
        private void a1_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            a1.Background = Brushes.LightGray;
        }

        private void a1_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
            a1.Background = Brushes.White;
        }

        private void a2_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            a2.Background = Brushes.LightGray;
        }

        private void a2_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
            a2.Background = Brushes.White;
        }
       
        private void a3_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            a3.Background = Brushes.LightGray;
        }

        private void a3_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
            a3.Background = Brushes.White;
        }

        private void b1_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            b1.Background = Brushes.LightGray;
        }

        private void b1_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
            b1.Background = Brushes.White;
        }

        private void b2_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            b2.Background = Brushes.LightGray;
        }

        private void b2_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
            b2.Background = Brushes.White;
        }

        private void b3_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            b3.Background = Brushes.LightGray;
        }

        private void b3_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
            b3.Background = Brushes.White;
        }

        private void c1_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            c1.Background = Brushes.LightGray;
        }

        private void c1_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
            c1.Background = Brushes.White;
        }

        private void c2_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            c2.Background = Brushes.LightGray;
        }

        private void c2_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
            c2.Background = Brushes.White;
        }

        private void c3_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            c3.Background = Brushes.LightGray;
        }

        private void c3_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
            c3.Background = Brushes.White;
        }
        #endregion

        #region Click Event
        private void a1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (ZmienneG.turn)
            {
                case 1:
                    a1.Text = "X";
                    ZmienneG.turn = 2;
                    Komunikat("Ruch: " + ZmienneG.playerO);
                    ZmienneG.field[0] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                case 2:
                    a1.Text = "O";
                    ZmienneG.turn = 1;
                    Komunikat("Ruch: " + ZmienneG.playerX);
                    ZmienneG.field[0] = "O";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                default:
                    break;
            }
           
        }

        private void a2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (ZmienneG.turn)
            {
                case 1:
                    a2.Text = "X";
                    ZmienneG.turn = 2;
                    Komunikat("Ruch: " + ZmienneG.playerO);
                    ZmienneG.field[1] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                case 2:
                    a2.Text = "O";
                    ZmienneG.turn = 1;
                    Komunikat("Ruch: " + ZmienneG.playerX);
                    ZmienneG.field[1] = "O";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                default:
                    break;
            }
        }

        private void a3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (ZmienneG.turn)
            {
                case 1:
                    a3.Text = "X";
                    ZmienneG.turn = 2;
                    Komunikat("Ruch: " + ZmienneG.playerO);
                    ZmienneG.field[2] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                case 2:
                    a3.Text = "O";
                    ZmienneG.turn = 1;
                    Komunikat("Ruch: " + ZmienneG.playerX);
                    ZmienneG.field[2] = "O";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                default:
                    break;
            }
        }

        private void b1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (ZmienneG.turn)
            {
                case 1:
                    b1.Text = "X";
                    ZmienneG.turn = 2;
                    Komunikat("Ruch: " + ZmienneG.playerO);
                    ZmienneG.field[3] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                case 2:
                    b1.Text = "O";
                    ZmienneG.turn = 1;
                    Komunikat("Ruch: " + ZmienneG.playerX);
                    ZmienneG.field[3] = "O";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                default:
                    break;
            }
        }

        private void b2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (ZmienneG.turn)
            {
                case 1:
                    b2.Text = "X";
                    ZmienneG.turn = 2;
                    Komunikat("Ruch: " + ZmienneG.playerO);
                    ZmienneG.field[4] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                case 2:
                    b2.Text = "O";
                    ZmienneG.turn = 1;
                    Komunikat("Ruch: " + ZmienneG.playerX);
                    ZmienneG.field[4] = "O";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                default:
                    break;
            }
        }

        private void b3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (ZmienneG.turn)
            {
                case 1:
                    b3.Text = "X";
                    ZmienneG.turn = 2;
                    Komunikat("Ruch: " + ZmienneG.playerO);
                    ZmienneG.field[5] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                case 2:
                    b3.Text = "O";
                    ZmienneG.turn = 1;
                    Komunikat("Ruch: " + ZmienneG.playerX);
                    ZmienneG.field[5] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                default:
                    break;
            }
        }

        private void c1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (ZmienneG.turn)
            {
                case 1:
                    c1.Text = "X";
                    ZmienneG.turn = 2;
                    Komunikat("Ruch: " + ZmienneG.playerO);
                    ZmienneG.field[6] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                case 2:
                    c1.Text = "O";
                    ZmienneG.turn = 1;
                    Komunikat("Ruch: " + ZmienneG.playerX);
                    ZmienneG.field[6] = "O";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                default:
                    break;
            }
        }

        private void c2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (ZmienneG.turn)
            {
                case 1:
                    c2.Text = "X";
                    ZmienneG.turn = 2;
                    Komunikat("Ruch: " + ZmienneG.playerO);
                    ZmienneG.field[7] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                case 2:
                    c2.Text = "O";
                    ZmienneG.turn = 1;
                    Komunikat("Ruch: " + ZmienneG.playerX);
                    ZmienneG.field[7] = "O";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                default:
                    break;
            }
        }

        private void c3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (ZmienneG.turn)
            {
                case 1:
                    c3.Text = "X";
                    ZmienneG.turn = 2;
                    Komunikat("Ruch: " + ZmienneG.playerO);
                    ZmienneG.field[8] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);
                    break;
                case 2:
                    c3.Text = "O";
                    ZmienneG.turn = 1;
                    ZmienneG.field[8] = "X";
                    FunkcjePomocnicze.Check(ZmienneG.field);

                    Komunikat("Ruch: " + ZmienneG.playerX);
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void Game()
        {   if(ZmienneG.turn == 0)
            {
                FunkcjePomocnicze.Losuj(nameS1.Text, nameS2.Text);
                Komunikat(ZmienneG.playerX, 1);
                Komunikat(ZmienneG.playerO, 2);
                Komunikat("Twój ruch: " + ZmienneG.playerO);
                ZmienneG.turn = 2;
            }
            
        }

        private void Komunikat(string tekst, int Switch = 0)
        {
            switch (Switch)
            {
                case 0:
                    komunikat.Text = "> " + tekst;
                    break;

                case 1:
                    komunikatX.Text = "X: " + tekst;
                    break;

                case 2:
                    komunikatO.Text = "O: " + tekst;
                    break;
            }
        }


    }

}
