﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolkoKrzyzyk
{
    static class FunkcjePomocnicze
    {
        public static void Losuj(string p1, string p2)
        {
            Random r = new Random();
            if(r.Next(1, 1000) % 2 == 0)
            {
                ZmienneG.playerX = p1;
                ZmienneG.playerO = p2;
            }
            else
            {
                ZmienneG.playerX = p2;
                ZmienneG.playerO = p1;
            }
        }

        public static int Check(string[] field)
        {
            if (field[0] == field[1] && field[1] == field[2]) { return 1; }
            else if (field[3] == field[4] && field[4] == field[5]) { return 1; }
            else if (field[6] == field[7] && field[7] == field[8]) { return 1; }
            else if(field[0] == field[4] && field[4] == field[8]) { return 1; }
            else if (field[2] == field[4] && field[4] == field[6]) { return 1; }
            else if (field[0] == field[3] && field[3] == field[6]) { return 1; }
            else if (field[1] == field[4] && field[4] == field[7]) { return 1; }
            else if (field[2] == field[5] && field[5] == field[8]) { return 1; }
            else { return 0; }
        }
    }
}
